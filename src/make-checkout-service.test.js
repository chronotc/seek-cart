const { checkoutService } = require('./context');

describe('checkout', () => {
  it('should calculate the total excluding discounts', () => {
    const total = checkoutService.total({
      items: [
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false
          }
        }
      ],
      discounts: [
        {
          type: 'OVERRIDE',
          target: 'standout',
          value: '299.99'
        }
      ]
    });

    expect(total).toEqual(expect.objectContaining({
      subTotal: '592.98'
    }));
  });

  it('should calculate the total including discounts', () => {
    const total = checkoutService.total({
      items: [
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false
          }
        }
      ],
      discounts: [
        {
          type: 'OVERRIDE',
          target: 'standout',
          value: '299.99'
        }
      ]
    });

    expect(total).toEqual(expect.objectContaining({
      total: '569.98'
    }));
  });

  it('should calculate the total when there are no discounts', () => {
    const total = checkoutService.total({
      items: [
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false
          }
        }
      ],
      discounts: []
    });

    expect(total).toEqual(expect.objectContaining({
      total: '592.98'
    }));
  });
});
