const makeCartService = ({ productService, userDiscountService, checkoutService }) => ({ userId }) => {
  console.log(`Generating cart for user: ${userId}`);
  const discounts = userDiscountService.get(userId);
  let items = [];

  return {
    list: () => {
      console.log('items', items);
    },
    add: productId => {
      const product = productService.get(productId);
      if (!product) {
        throw new Error('Invalid Product');
      }

      items = items.concat(product);
    },
    total: () => {
      const { total, subTotal } = checkoutService.total({ items, discounts });
      console.log('items:', JSON.stringify(items, null, 2));
      console.log(`The subTotal is: ${subTotal}`);
      console.log(`The total is: ${total}`);
      return total;
    }
  };
};

module.exports = {
  makeCartService
};
