const _ = require('lodash');

const USERS_DISCOUNTS = {
  SecondBite: ['BUNDLE_CLASSIC_3FOR2'],
  AxilCoffeeRoasters: ['OVERRIDE_STANDOUT_299'],
  MYER: ['OVERRIDE_PREMIUM_389', 'BUNDLE_STANDOUT_5FOR4']
};

const makeGet =
({ discountService }) =>
  (id) => {
    const discountIds = _.get(USERS_DISCOUNTS, id);

    if (!discountIds) {
      return [];
    }

    const allDiscounts = discountService.getAll();

    return allDiscounts.filter(discount => discountIds.includes(discount.id));
  };

const makeUserDiscountService = ({ discountService }) => {
  return {
    get: _.memoize(makeGet({ discountService }))
  };
};

module.exports = {
  makeUserDiscountService
};
