const { userDiscountService } = require('./context');

describe('UserDiscountService', () => {
  describe('get', () => {
    it('should retrieve all discounts the user has access to', async () => {
      const discounts = userDiscountService.get('MYER');

      expect(discounts).toEqual([
        {
          id: 'BUNDLE_STANDOUT_5FOR4',
          type: 'BUNDLE',
          target: 'standout',
          value: {
            discountAt: 5,
            freeOfCharge: 1
          }
        },
        {
          id: 'OVERRIDE_PREMIUM_389',
          type: 'OVERRIDE',
          target: 'premium',
          value: '389.99'
        }
      ]);
    });

    it('should return an empty list of discounts if the user does not have discounts', async () => {
      const discounts = userDiscountService.get('nobody');

      expect(discounts).toEqual([]);
    });
  });
});
