const _ = require('lodash');

const DISCOUNTS = {
  BUNDLE_CLASSIC_3FOR2: {
    type: 'BUNDLE',
    target: 'classic',
    value: {
      discountAt: 3,
      freeOfCharge: 1
    }
  },
  BUNDLE_STANDOUT_5FOR4: {
    type: 'BUNDLE',
    target: 'standout',
    value: {
      discountAt: 5,
      freeOfCharge: 1
    }
  },
  OVERRIDE_STANDOUT_299: {
    type: 'OVERRIDE',
    target: 'standout',
    value: '299.99'
  },
  OVERRIDE_PREMIUM_389: {
    type: 'OVERRIDE',
    target: 'premium',
    value: '389.99'
  }
};

const applyOverride = ({ items, discount }) => {
  return items.map(item => {
    const hasDiscountApplied = _.get(item, 'price.appliedDiscount');

    if (hasDiscountApplied) {
      return item;
    }

    const isDiscountedItem = _.get(discount, 'target') === item.id;
    const total = isDiscountedItem
      ? _.get(discount, 'value')
      : _.get(item, 'price.amount');

    const appliedDiscount = isDiscountedItem
      ? _.get(discount, 'id')
      : null;

    return _.merge(
      {},
      item,
      {
        price: {
          total,
          appliedDiscount
        }
      }
    );
  });
};

const applyAllOverrides = ({ items, discounts }) => {
  const overrideDiscounts = discounts.filter(({ type }) => type === 'OVERRIDE');
  const itemsWithOverrides = overrideDiscounts.reduce((lineItems, discount) => {
    return applyOverride({ items: lineItems, discount });
  }, items);

  return {
    items: itemsWithOverrides,
    discounts: discounts.filter(({ type }) => type !== 'OVERRIDE')
  };
};

const applyAllBundles = ({ items, discounts }) => {
  const bundleDiscounts = discounts.filter(({ type }) => type === 'BUNDLE');
  const itemsWithBundle = bundleDiscounts.reduce(
    (lineItems, discount) => applyBundle({ items: lineItems, discount }),
    items
  );

  return {
    items: itemsWithBundle,
    discounts: discounts.filter(({ type }) => type !== 'BUNDLE')
  };
};

const applyBundle = ({ items, discount }) => {
  const discountTarget = _.get(discount, 'target');
  const applicable = items.filter(item => item.id === discountTarget);
  const discountAt = _.get(discount, 'value.discountAt') - 1;

  const numberOfBundles = Math.floor(applicable.length / discountAt);
  if (!numberOfBundles) {
    return items;
  }

  const freeOfCharge = _.get(discount, 'value.freeOfCharge');
  const positions = items.reduce(
    (acc, item, index) => {
      if (item.id !== discountTarget) {
        return acc;
      }

      const { matchingProductIndex, applyFreeToIndex } = acc;
      if (index % discountAt === 0 && index !== 0) {
        const additionFreeItems = _.take(matchingProductIndex.concat(index), freeOfCharge);
        return {
          applyFreeToIndex: applyFreeToIndex.concat(additionFreeItems),
          matchingProductIndex: []
        };
      }

      const updatedMatchingProductIndex = matchingProductIndex.concat(index);
      return {
        applyFreeToIndex,
        matchingProductIndex: updatedMatchingProductIndex
      };
    },
    {
      applyFreeToIndex: [],
      matchingProductIndex: []
    });

  return items.map((item, index) => {
    if (positions.applyFreeToIndex.includes(index)) {
      return _.merge(
        {},
        item,
        {
          price: {
            total: '0',
            appliedDiscount: discount.id
          }
        }
      );
    }

    return _.merge(
      {},
      item,
      {
        price: {
          total: _.get(item, 'price.amount'),
          appliedDiscount: null
        }
      }
    );
  });
};

const apply = ({ items, discounts }) => {
  const processedOverrides = applyAllOverrides({ items, discounts });
  const processedBundles = applyAllBundles({
    items: processedOverrides.items,
    discounts: processedOverrides.discounts
  });

  return processedBundles;
};

const getAll = () => {
  return Object.entries(DISCOUNTS).map(([key, value]) => {
    return {
      id: key,
      ...value
    };
  });
};

const makeDiscountService = () => {
  return {
    apply,
    getAll
  };
};

module.exports = {
  makeDiscountService
};
