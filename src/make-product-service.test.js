const { productService } = require('./context');

describe('ProductService', () => {
  describe('get', () => {
    it('should retrieve a specific product', async () => {
      const product = await productService.get('classic');
      expect(product).toEqual(
        expect.objectContaining({
          id: expect.any(String),
          name: 'Classic'
        })
      );
    });

    it('should return nothing if the product is not found', async () => {
      const product = await productService.get('doesnotexist');
      expect(product).toEqual(null);
    });
  });

  describe('getAll', () => {
    it('should retrieve all products', async () => {
      const products = await productService.getAll();
      expect(products).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(String),
            name: 'Classic'
          }),
          expect.objectContaining({
            id: expect.any(String),
            name: 'Standard'
          }),
          expect.objectContaining({
            id: expect.any(String),
            name: 'Premium'
          })
        ])
      );
    });
  });
});
