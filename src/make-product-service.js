const _ = require('lodash');

const PRODUCTS = {
  classic: {
    name: 'Classic',
    price: {
      amount: '269.99',
      taxInclusive: false
    }
  },
  standout: {
    name: 'Standard',
    price: {
      amount: '322.99',
      taxInclusive: false
    }
  },
  premium: {
    name: 'Premium',
    price: {
      amount: '394.99',
      taxInclusive: false
    }
  }
};

const get = (id) => {
  const product = _.get(PRODUCTS, id);

  if (!product) {
    return null;
  }

  return {
    id,
    ...product
  };
};

const getAll = () => {
  return Object.entries(PRODUCTS).map(([key, value]) => {
    return {
      id: key,
      ...value
    };
  });
};

const makeProductService = () => {
  return {
    get,
    getAll
  };
};

module.exports = {
  makeProductService
};
