const { makeProductService } = require('./make-product-service');
const { makeCheckoutService } = require('./make-checkout-service');
const { makeDiscountService } = require('./make-discount-service');
const { makeUserDiscountService } = require('./make-user-discount-service');
const { makeCartService } = require('./make-cart-service');

const discountService = makeDiscountService();
const productService = makeProductService();
const userDiscountService = makeUserDiscountService({ discountService });
const checkoutService = makeCheckoutService({ discountService });

module.exports = {
  generateCart: makeCartService({
    userDiscountService,
    productService,
    checkoutService
  }),
  discountService,
  userDiscountService,
  productService,
  checkoutService
};
