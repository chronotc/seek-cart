const { generateCart } = require('./src/context');

const cart = generateCart({ userId: 'default' });
cart.add('classic');
cart.add('classic');
cart.total();

const secondBiteCart = generateCart({ userId: 'SecondBite' });
secondBiteCart.add('classic');
secondBiteCart.add('classic');
secondBiteCart.add('classic');
secondBiteCart.add('premium');
secondBiteCart.total();

const axilCart = generateCart({ userId: 'AxilCoffeeRoasters' });
axilCart.add('standout');
axilCart.add('standout');
axilCart.add('standout');
axilCart.add('premium');
axilCart.total();
