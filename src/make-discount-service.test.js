const { discountService } = require('./context');

describe('DiscountService', () => {
  describe('getAll', () => {
    it('should retrieve all discounts', async () => {
      const products = await discountService.getAll();
      expect(products).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: 'BUNDLE_CLASSIC_3FOR2',
            type: 'BUNDLE',
            target: 'classic'
          }),
          expect.objectContaining({
            id: 'OVERRIDE_STANDOUT_299',
            type: 'OVERRIDE',
            target: 'standout',
            value: '299.99'
          })
        ])
      );
    });
  });
});
