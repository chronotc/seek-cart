const numeral = require('numeral');
const _ = require('lodash');

const getSubTotal = ({ items = [] }) => {
  const subTotalNumeral = items.reduce((acc, item) => {
    const price = _.get(item, 'price.amount') || 0;
    return numeral(acc).add(price);
  }, 0);

  return subTotalNumeral.format('0.00');
};

const getTotal = ({ items = [] }) => {
  const totalNumeral = items.reduce((acc, item) => {
    const price = _.get(item, 'price.total') || _.get(item, 'price.amount');
    return numeral(acc).add(price);
  }, 0);

  return totalNumeral.format('0.00');
};

const makeGetTotal =
({ discountService }) =>
  ({ items = [], discounts = [] }) => {
    const lineItemsWithDiscount = discountService.apply({ items, discounts });

    return {
      subTotal: getSubTotal({ items }),
      total: getTotal({ items: lineItemsWithDiscount.items })
    };
  };

const makeCheckoutService = ({ discountService }) => {
  return {
    total: makeGetTotal({ discountService })
  };
};

module.exports = {
  makeCheckoutService
};
