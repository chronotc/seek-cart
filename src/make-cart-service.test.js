const { generateCart } = require('./context');

describe('cartService', () => {
  it('should calculate the total inside a cart', () => {
    const axilCart = generateCart({ userId: 'AxilCoffeeRoasters' });
    axilCart.add('standout');
    axilCart.add('standout');
    axilCart.add('standout');
    axilCart.add('premium');

    expect(axilCart.total()).toEqual('1294.96');
  });
});
