# seek-cart

## Prerequisites

Node.js - 12.14.0

Yarn - https://yarnpkg.com/

## Overview

This is a checkout system that will support multiple products.

Initially 3 products are supported.

Example:

```
Classic - $269.99
Stand out - $322.99
Premium - $394.99
```

Discounts can be applied to specific customers.

Example:

```
3 for 2 deal - Classic
Stand out discounted to 299.99
5 for 4 deal - Stand out
Premium discounted to 389.99
```

- Volume based
- Fixed discount per item

Discount codes:
BUNDLE_CLASSIC_3FOR2
BUNDLE_STANDOUT_5FOR4
OVERRIDE_STANDOUT_299
OVERRIDE_PREMIUM_389

Customer with discounts:
SecondBite - [BUNDLE_CLASSIC_3FOR2]
Axil Coffee Roasters - [OVERRIDE_STANDOUT_299]
MYER - [BUNDLE_STANDOUT_5FOR4,OVERRIDE_PREMIUM_389]

Sample Test Cases

```
Customer: default
Items: `classic`, `standout`, `premium`
Total: $987.97
Customer: SecondBite
Items: `classic`, `classic`, `classic`, `premium`
Total: $934.97
Customer: Axil Coffee Roasters
Items: `standout`, `standout`, `standout`, `premium`
Total: $1294.96
```

## Limitations

- Bundles and overrides cannot be applied to the same shopping cart

## Commands

Running tests

```js
yarn test
```

Running list

```js
yarn lint
```

Running sample cases

```js
yarn start
```