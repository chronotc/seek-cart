const { discountService } = require('./context');

describe('discount.applyOverride', () => {
  let processed;
  beforeAll(() => {
    processed = discountService.apply({
      items: [
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false
          }
        },
        {
          id: 'premium',
          name: 'Premium',
          price: {
            amount: '394.99',
            taxInclusive: false
          }
        }
      ],
      discounts: [
        {
          id: 'OVERRIDE_STANDOUT_299.99',
          type: 'OVERRIDE',
          target: 'standout',
          value: '299.99'
        },
        {
          id: 'OVERRIDE_PREMIUM_389.99',
          type: 'OVERRIDE',
          target: 'premium',
          value: '389.99'
        }
      ]
    });
  });

  it('should not apply discount on undiscounted items', () => {
    expect(processed.items).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false,
            total: '269.99',
            appliedDiscount: null
          }
        })
      ])
    );
  });

  it('should handle 2 or more override discounts', () => {
    expect(processed.items).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false,
            total: '299.99',
            appliedDiscount: 'OVERRIDE_STANDOUT_299.99'
          }
        })
      ])
    );

    expect(processed.items).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: 'premium',
          name: 'Premium',
          price: {
            amount: '394.99',
            taxInclusive: false,
            total: '389.99',
            appliedDiscount: 'OVERRIDE_PREMIUM_389.99'
          }
        })
      ])
    );
  });

  it('should exclude processed discounts', () => {
    expect(processed.discounts).toEqual([]);
  });
});
