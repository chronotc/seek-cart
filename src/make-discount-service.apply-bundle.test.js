const { discountService } = require('./context');

describe('discount', () => {
  let processed;

  beforeAll(() => {
    processed = discountService.apply({
      items: [
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'standout',
          name: 'Standard',
          price: {
            amount: '322.99',
            taxInclusive: false
          }
        },
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        },
        {
          id: 'classic',
          name: 'Classic',
          price: {
            amount: '269.99',
            taxInclusive: false
          }
        }
      ],
      discounts: [
        {
          id: 'BUNDLE_CLASSIC_3FOR2',
          type: 'BUNDLE',
          target: 'classic',
          value: {
            discountAt: 3,
            freeOfCharge: 1
          }
        }
      ]
    });
  });

  it('should calculate bundle discounts', () => {
    expect(processed.items).toEqual([
      {
        id: 'classic',
        name: 'Classic',
        price: {
          amount: '269.99',
          taxInclusive: false,
          total: '0',
          appliedDiscount: 'BUNDLE_CLASSIC_3FOR2'
        }
      },
      {
        id: 'classic',
        name: 'Classic',
        price: {
          amount: '269.99',
          taxInclusive: false,
          total: '269.99',
          appliedDiscount: null
        }
      },
      {
        id: 'standout',
        name: 'Standard',
        price: {
          amount: '322.99',
          taxInclusive: false,
          total: '322.99',
          appliedDiscount: null
        }
      },
      {
        id: 'classic',
        name: 'Classic',
        price: {
          amount: '269.99',
          taxInclusive: false,
          total: '269.99',
          appliedDiscount: null
        }
      },
      {
        id: 'classic',
        name: 'Classic',
        price: {
          amount: '269.99',
          taxInclusive: false,
          total: '269.99',
          appliedDiscount: null
        }
      }
    ]);
  });

  it('should exclude processed discounts', () => {
    expect(processed.discounts).toEqual([]);
  });
});
